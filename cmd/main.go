package main

import (
	"log"
	"net"

	"gitlab.com/movieapp4/rating/genproto/movie"
	"gitlab.com/movieapp4/rating/internal/controller/rating"
	grpchandler "gitlab.com/movieapp4/rating/internal/handler/grpc"
	"gitlab.com/movieapp4/rating/internal/repository/memory"
	"google.golang.org/grpc"
)

const serviceName = "rating"

func main() {
	log.Println("Starting the movie rating service")
	repo := memory.New()
	ctrl := rating.NewController(repo)
	h := grpchandler.New(ctrl)

	lis, err := net.Listen("tcp", "localhost:8082")
	if err != nil {
		log.Fatalf("failed to listen %v", err)
	}

	srv := grpc.NewServer()
	movie.RegisterRatingSerivceServer(srv, h)
	if err := srv.Serve(lis); err != nil {
		panic(err)
	}

}

// func main() {
// 	var port int

// 	flag.IntVar(&port, "port", 8082, "API handler port")
// 	flag.Parse()

// 	log.Printf("Starting rating service on port %d\n", port)

// 	registry, err := consul.NewRegistry("localhost:8500")
// 	if err != nil {
// 		panic(err)
// 	}

// 	ctx := context.Background()
// 	instanceID := discovery.GenerateInstanceID(serviceName)

// 	if err := registry.Register(ctx, instanceID, serviceName, fmt.Sprintf("localhost:%d", port)); err != nil {
// 		panic(err)
// 	}

// 	go func() {
// 		for {
// 			if err := registry.ReportHealthyState(ctx, instanceID, serviceName); err != nil {
// 				log.Panicln("failed to report health state: " + err.Error())
// 			}
// 			time.Sleep(1 * time.Second)
// 		}
// 	}()

// 	defer registry.Deregister(ctx, instanceID, serviceName)

// 	repo := memory.New()
// 	ctrl := rating.NewController(repo)
// 	h := httphandler.NewHandler(ctrl)

// 	http.Handle("/rating", http.HandlerFunc(h.Handle))
// 	if err := http.ListenAndServe(fmt.Sprintf(":%d", port), nil); err != nil {
// 		panic(err)
// 	}
// }
